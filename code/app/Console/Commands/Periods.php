<?php

namespace rockwerchter\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use rockwerchter\Participant;
use rockwerchter\Period;
use rockwerchter\Http\Traits\MailTraits;

class Periods extends Command
{

    use MailTraits;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'period:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if the period is over';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get  last period
        $lastPeriod = Period::orderBy('end_date', 'desc')->where(function ($query){
            $query
                ->where('end_date', '=', Carbon::now()->subDay()->toDateString());
        })->first();

        if(!is_null($lastPeriod)){
            $participantPeriod = DB::table('participant_period')->select('id', 'participant_id', 'votes', 'period_id')
                ->where('period_id', $lastPeriod->id)->first();

            if(!is_null($participantPeriod)){
                $participantPeriodId = $participantPeriod->id;
                $participant = Participant::find($participantPeriod->participant_id);

                if(!is_null($participant)){
                    $this->sendWinner($participant);
                }
            }
        }

    }
}
