<?php

namespace rockwerchter\Console\Commands;

use Illuminate\Console\Command;
use rockwerchter\Http\Traits\MailTraits;

class sendParticipants extends Command
{

    use MailTraits;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:participants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends the participants of the day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->getParticipantsOfDay();
    }
}
