<?php

namespace rockwerchter\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Request;

use rockwerchter\Http\Requests;
use Laravel\Socialite\Contracts\Factory as Socialite;
use rockwerchter\Participant;
use rockwerchter\Vote;

class AuthController extends Controller
{
    /**
     * AuthController constructor.
     * @param Socialite $socialite
     */
    public function __construct(Socialite $socialite){
        $this->socialite = $socialite;
    }


    /**
     * @param null $provider
     *
     *
     * @return mixed
     */
    public function getSocialAuth($provider = null)
    {
        if(Session::has('isVoting')){
            Session::remove('isVoting');
        }
        if(!config("services.$provider")) abort('404'); //just to handle providers that doesn't exist

        return $this->socialite->with($provider)->redirect();
    }


    /**
     * @param null $provider
     *
     * @return string
     */
    public function getSocialAuthCallback($provider=null)
    {

        // check if User is Voting
        if(Session::has('isVoting')){
            $providerUser = $this->socialite->driver('facebook')->user();
            $postID = Session::get('isVoting');
            $this->addVote($providerUser, $postID);

            // RETURN THE SHOW POST VIEW
            return redirect()->route('show', $postID)->withErrors('test');
        }

        // OTHERWISE -> REGISTERING

        // when facebook call us a with token
        $providerUser = $this->socialite->driver('facebook')->user();

        $participantIpAdress = Request::ip();

        if(is_null($providerUser)){
            return redirect()->action('ParticipantController@register')->withErrors(['Oeps! Er ging iets mis bij het registreren. Probeer het later nog eens.']);
        }
        else{
            if(is_null($participantIpAdress)){
                return redirect()->action('ParticipantController@showRegisterForm')->withErrors(['Oeps! Er ging iets mis bij het registreren. Probeer het later nog eens.']);
            }
            else{
                $participant = new Participant();

                $participant->first_name    = $providerUser->user['name'];
                $participant->email         = $providerUser->user['email'];
                $participant->ip_address    = $participantIpAdress;

                Session::put('currentParticipant', $participant);

                return redirect()->route('showArtistForm');
            }
        }
    }

    /**
     * @param null $provider
     *
     *
     * @return mixed
     */
    public function getSocialVote($provider = null, $post)
    {
        if(!config("services.$provider")) abort('404'); //just to handle providers that doesn't exist

        Session::put('isVoting', $post);
        return $this->socialite->with($provider)->redirect();
    }

    protected function addVote($v, $postID){

        // check if already voted
        $voter = DB::table('votes')->select('user_email')
            ->where('user_email', $v->email)->where('participant_period_id', $postID)->first();


        $postID = Session::get('isVoting');
        Session::remove('isVoting');

        if(is_null($voter)){
            $vote = new Vote();
            $vote->user_email = $v->email;
            $vote->participant_period_id = $postID;

            $vote->save();

            DB::table('participant_period')->where('id', $postID)
                ->increment('votes');

            return false;
        }
        else{

            return true;
        }


    }

}
