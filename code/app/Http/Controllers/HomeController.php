<?php

namespace rockwerchter\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use rockwerchter\Participant;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participants = Participant::withTrashed()->get();

        return view('admin.participants', compact('participants'));
    }

    /**
     * soft delete participant
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggleParticipant($id){
        $participant = Participant::withTrashed()->find($id);

        if(is_null($participant->deleted_at)) $participant->delete();
        else $participant->restore();

        return back();
    }

    /**
     * export Participants Excel.
     *
     * @return \Illuminate\Http\Response
     */
    public function participants()
    {
        $participants = Participant::get();

        $participantSheet = Excel::create('participants_' . Carbon::today()->toDateString('d M Y'), function($excel) use($participants) {

            $excel->sheet('Sheetname', function($sheet) use($participants) {

                $sheet->fromArray($participants);

            });

        })->export('xls');

        return back();
    }
}
