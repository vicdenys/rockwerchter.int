<?php

namespace rockwerchter\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use rockwerchter\Http\Requests;
use rockwerchter\Http\Requests\ParticipantRequest;
use rockwerchter\Participant;
use rockwerchter\Period;

class ParticipantController extends Controller
{
    //

    /**
     * Get The Form for registering participants
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegisterForm()
    {
        if(Session::has('currentParticipant')){

            if (!$this->checkParticipant()){
                Session::forget('currentParticipant');
                return view('participant.register')->withErrors(['Dit emailadres heeft al deelgenomen deze periode! Probeer volgende periode opnieuw']);
            }

            return redirect()->action('ParticipantController@showArtistForm');
        }
        else{
            return view('participant.register');
        }
    }

    /**
     * Show specific post
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id){
        $participation = DB::table('participant_period')->select('period_id', 'participant_id',  'artists')
            ->where('id', $id)->first();

        $votes = DB::table('votes')->where('participant_period_id', $id)->get()->count();
        $participationId = $id;

        $artists = explode("|", $participation->artists);
        array_pop($artists);

        $participant = Participant::where('id', $participation->participant_id)->first();

        return view('participant.show', compact('artists', 'participant', 'participationId', 'votes'));
    }

    /**
     * Register a participant
     *
     * @param Request|ParticipantRequest $request
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register(ParticipantRequest $request)
    {
        $participantIpAdress = $request->getClientIp();

        if(!is_null($participantIpAdress)){
            $participant = new Participant();

            $participant->first_name    = $request->first_name;
            $participant->last_name     = $request->last_name;
            $participant->address       = $request->address;
            $participant->city          = $request->city;
            $participant->email         = strtolower($request->email);
            $participant->ip_address    = $participantIpAdress;

            Session::put('currentParticipant', $participant);

            return redirect()->route('showArtistForm');
        }
        else{
            return back()->withErrors(['Oeps! Er ging iets mis bij het registreren. Probeer het later nog eens.'])->withInput();
        }
    }

    /**
     * get the form to select artists
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showArtistForm(){
        if(Session::has('currentParticipant')){

            if (!$this->checkParticipant()){
                Session::forget('currentParticipant');
                return redirect()->action('ParticipantController@showRegisterForm')->withErrors(['Dit emailadres heeft al deelgenomen deze periode! Probeer volgende periode opnieuw']);
            }

            $participant = Session::get('currentParticipant')->toArray();

            return view('participant.artists', compact('participant'));
        }
        else{
            return redirect()->action('ParticipantController@showRegisterForm');
        }
    }

    /**
     * Cancel the registration by deleting from sessaion
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelRegister(){
        if(Session::has('currentParticipant')){
            Session::forget('currentParticipant');
            return redirect()->action('ParticipantController@showRegisterForm');
        }
        else{
            return redirect()->action('ParticipantController@showRegisterForm');
        }
    }

    /**
     * save Participant
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveParticipant(Request $request){
        $participant = Session::get('currentParticipant');
        $participant->save();

        $currentPeriod = $this->getCurrentPeriod();

        $participant = Participant::where('email', '=', $participant->email)->first();
        $participant->periods()->attach($currentPeriod->id, ['artists' => $request->lineUp]);

        Session::forget('currentParticipant');

        $participation = DB::table('participant_period')->select('id')
            ->where('participant_id', $participant->id)->first();

        return redirect()->route('show', $participation->id);
    }

    /**
     * Check if participant already participated this period
     *
     * @return bool
     */
    protected function checkParticipant(){
        //Check if user already participated this year
        $participant = Session::get('currentParticipant');
        // Get First Coming Period or Current;
        $currentPeriod = $this->getCurrentPeriod();

        $participantThisPeriod = $currentPeriod->participants->where('email', $participant->email)->first();

        // Check if user already played this period
        if(is_null($participantThisPeriod)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the Current period
     *
     * @return mixed
     */
    protected function getCurrentPeriod(){
        return $currentPeriod = Period::orderBy('start_date', 'asc')->where(function ($query){
            $query
                ->where('start_date', '<=', Carbon::now()->toDateString())
                ->where('end_date', '>=', Carbon::now()->toDateString());
        })->first();
    }

}
