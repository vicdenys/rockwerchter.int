<?php

namespace rockwerchter\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use rockwerchter\Participant;
use rockwerchter\Period;

class UserController extends Controller
{
    public function index(){
        // Get participants with high scores
        $participant =  null;
        $participantPeriodId = null;

        // get  last period
        $lastPeriod = Period::orderBy('end_date', 'desc')->where(function ($query){
            $query
                ->where('end_date', '<=', Carbon::yesterday()->toDateString());
        })->first();

        //get current period
        $currentPeriod = Period::orderBy('start_date', 'asc')->where(function ($query){
            $query
                ->where('start_date', '<=', Carbon::now()->toDateString())
                ->where('end_date', '>=', Carbon::now()->toDateString());
        })->first();

        if(!is_null($lastPeriod)){
            $participantPeriod = DB::table('participant_period')->select('id', 'participant_id', 'votes', 'period_id')
                ->where('period_id', $lastPeriod->id)->first();

            if(!is_null($participantPeriod)){
                $participantPeriodId = $participantPeriod->id;
                $participant = Participant::find($participantPeriod->participant_id);
            }
        }



        //$participants = $participation = DB::table('votes')->select('period_id', 'participant_id',  'artists')
          // ->where('id', $id)->first();

        return view('welcome', compact('participant', 'participantPeriodId', 'currentPeriod'));
    }
}
