<?php

namespace rockwerchter\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParticipantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'address' => 'required|max:255',
            'city' => 'required|max:255',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => 'U moet een voornaam ingeven.',
            'first_name.max'  => 'Uw voornaam kan niet langer zijn dan 255 karakters.',
            'last_name.required'  => 'U moet een naam ingeven.',
            'last_name.max'  => 'Uw naam kan niet langer zijn dan 255 karakters',
            'email.required' => 'U moet een email ingeven.',
            'email.max'  => 'Uw email kan niet langer zijn dan 255 karakters.',
            'email.email'  => 'U moet een geldig e-mailadres ingeven.',
            'email.unique'  => 'Dit e-mailadres is al eens gebruikt.',
            'address.required' => 'U moet een straatnaam en huisnummer ingeven.',
            'address.max'  => 'Uw straatnaam en huisnummer kan niet langer zijn dan 255 karakters.',
            'city.required' => 'U moet een plaatsnaam ingeven.',
            'city.max'  => 'Uw plaatsnaam kan niet langer zijn dan 255 karakters.',
        ];
    }
}
