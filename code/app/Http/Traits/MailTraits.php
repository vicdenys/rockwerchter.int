<?php

namespace rockwerchter\Http\Traits;

use rockwerchter\Participant;
use Carbon\Carbon;
use rockwerchter\User;
use Excel;
use Mail;

trait MailTraits {

    public function getParticipantsOfDay()
    {
        $title = 'Rock Werchter - Admin';
        $content = 'In de bijlage vind je een xls bestand met de deelnemers van vanddag ('. Carbon::yesterday()->toDateString() .')';
        $email = User::first()->email;

        $participants = Participant::where('created_at', '>=', Carbon::yesterday())->get();

        $fileName = 'Participants_' . Carbon::yesterday()->toDateString();

        $participantSheet = Excel::create($fileName, function($excel) use($participants) {

            $excel->sheet('Sheetname', function($sheet) use($participants) {

                $sheet->fromArray($participants);

            });

        })->store('xls' , storage_path('exports/participants'));

        $sheetFileName = $participantSheet->storagePath . '/' . $fileName . '.xls' ;

        Mail::queue('mail.send', ['title' => $title, 'content' => $content], function ($message) use($sheetFileName, $email)
        {
            $message->from('admin@rockwerchter.vicdenys.be', 'Rock erchter - Admin');
            $message->to($email);
            $message->subject('Participants of the day');
            $message->attach($sheetFileName);

        });

        return back();
    }

    public function sendWinner($user)
    {
        $email = User::first()->email;
        $emailWinner = $user->email;
        $title = $user['first_name'] . ' ' . $user['last_name'] . ' has won today';
        $content = $user['first_name'] . ' ' . $user['last_name'] . ' has won. You can contact him on ' . $user['email'];


        Mail::queue('mail.winner', ['title' => $title, 'content' => $content], function ($message) use($email)
        {
            $message->from('admin@rockwerchter.vicdenys.be', 'Rock Werchter - Admin');
            $message->to($email);
            $message->subject('Winner of the period');
        });

        Mail::queue('mail.winner', ['title' => 'You won combi-tickets for rock werchter 2017', 'content' => 'We will contact you soon'], function ($message) use($emailWinner)
        {
            $message->from('admin@rockwerchter.vicdenys.be', 'Rock Werchter- Admin');
            $message->to($emailWinner);
            $message->subject('You won rock werchter');
        });
    }
}
