<?php

namespace rockwerchter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Participant extends Model
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'address', 'city', 'ip_address'
    ];

    /**
     * Get the period associated with the participant.
     *
     */
    public function periods()
    {
        return $this->belongsToMany('rockwerchter\Period');
    }
}
