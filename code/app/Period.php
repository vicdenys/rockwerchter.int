<?php

namespace rockwerchter;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    /**
     * Get the participants associated with the period.
     */
    public function participants()
    {
        return $this->belongsToMany('rockwerchter\Participant');
    }
}
