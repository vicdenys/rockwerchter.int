<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant_period', function ( $table) {
            $table->increments('id');
            $table->integer('period_id')->unsigned();
            $table->integer('participant_id')->unsigned();
            $table->integer('votes')->default(0);
            $table->string('artists');
        });

        Schema::table('participant_period', function ( $table) {
            $table->foreign('period_id')
                ->references('id')->on('periods')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('participant_id')
                ->references('id')->on('participants')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('participant_period');
    }
}
