<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PeriodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('periods')->delete();

        $period = [
            [
                'name' => 'Periode 1',
                'start_date'  =>  Carbon::create(2016, 10, 3, 0, 0, 0),
                'end_date'     => Carbon::create(2016, 10, 20, 0, 0, 0)
            ]
        ];
        DB::table('periods')->insert($period);

        $period = [
            [
                'name' => 'Periode 2',
                'start_date'  =>  Carbon::create(2016, 10, 20, 0, 0, 0),
                'end_date'     => Carbon::create(2016, 10, 30, 0, 0, 0)
            ]
        ];
        DB::table('periods')->insert($period);

        $period = [
            [
                'name' => 'Periode 3',
                'start_date'  =>  Carbon::create(2016, 10, 30, 0, 0, 0),
                'end_date'     => Carbon::create(2016, 11, 10, 0, 0, 0)
            ]
        ];
        DB::table('periods')->insert($period);

        $period = [
            [
                'name' => 'Periode 4',
                'start_date'  =>  Carbon::create(2016, 11, 10, 0, 0, 0),
                'end_date'     => Carbon::create(2016, 12, 30, 0, 0, 0)
            ]
        ];
        DB::table('periods')->insert($period);
    }
}
