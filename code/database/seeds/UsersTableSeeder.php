<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $users = [
            [
                'name'  =>  'admin',
                'email'     => 'vic.denys@student.kdg.be',
                'password'  =>  Hash::make('admin'),

            ]
        ];

        DB::table('users')->insert($users);
    }
}
