
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

require('../../../node_modules/jquery/dist/jquery.min.js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

(function(){

    $(document).ready(function(){

        var artists,
            myLineUp = [];

        $('#submit').click(function(){
            post('savep', 'POST')
        });


        // Check if select artist page
        if($('.selectArtistWrapper').length){

            // call api to get Artists
            $.ajax({
                url: "http://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&api_key=670031344eb176d6ee18b77118aa4737&format=json",
                dataType: "jsonp"
            }).done(function(data){

                artists = data.artists.artist;

                var artistsHTML = '';

                for(artist in data.artists.artist){
                    artistsHTML += newArtistHTMLElement(artist, data.artists.artist[artist].name, data.artists.artist[artist].image[1]['#text'], 'out');
                }

                $('#artists').append(artistsHTML);

                $('.media.out').click( function () {
                    var artist = artists[$(this).attr('id')];

                    if (getObjectsize(myLineUp) < 8 && hasArtist(artist)){
                        $(this).addClass('selected');
                        addToLineUp(artist);
                        checkSubmitBtn();
                    }
                });
            });


            function addToLineUp(artist){
                myLineUp.push(artist);

                var artistHTML = newArtistHTMLElement('a' + (getObjectsize(myLineUp) - 1).toString(), artist.name, artist.image[1]['#text'], 'in');

                $('#myLineUp').append(artistHTML);

                $('#a' + (getObjectsize(myLineUp) - 1).toString()).click( function () {
                    var artist = myLineUp[$(this).attr('id').charAt(1)];

                    myLineUp.splice(getIndexOfArtist(artist, myLineUp), 1);
                    $('#' + getIndexOfArtist(artist, artists)).removeClass('selected');
                    $(this).remove();
                    resetId();

                    checkSubmitBtn();
                });
            }


            // Get object size
            function getObjectsize (obj) {
                var size = 0, key;
                for (key in obj) {
                    if (obj.hasOwnProperty(key)) size++;
                }
                return size;
            };

            function hasArtist(a){
                for(artist in myLineUp){
                    if (myLineUp[artist].name === a.name){
                        return false;
                    }
                }

                return true;
            }

            function getIndexOfArtist(a, obj) {
                for(artist in obj){
                    if (obj[artist].name === a.name){
                        return artist
                    }
                }

                return false;
            }

            function resetId(){
                $('#myLineUp > div').each(function(index){
                    $(this).attr('id', 'a' + index);
                });
            }

            function checkSubmitBtn(){
                if(getObjectsize(myLineUp) < 8){
                    $("#submit").attr('disabled', true);
                }
                else{
                    $("#submit").attr('disabled', false);
                }

                if(getObjectsize(myLineUp) == 0){
                    $("#no-artist").show();
                }
                else{
                    $("#no-artist").hide();
                }
            }

            function linupToString(){
                var artistString = '';
                for(artist in myLineUp){
                    artistString += myLineUp[artist].name + '|';
                }
                return artistString
            }

            function post(path, method) {

                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", 'lineUp');
                hiddenField.setAttribute("value", linupToString());

                $('#artistForm').append(hiddenField);

                $('#artistForm').submit();
                $('#artistForm').remove();
            }

        }

        // Check if select artist page
        if($('.showParticipant').length){

            var artists = getArtists();

            for(artist in artists){
                $.ajax({
                    url: "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=" + artists[artist].toLowerCase() + "&api_key=670031344eb176d6ee18b77118aa4737&format=json",
                    dataType: "jsonp"
                }).done(function(data){

                    console.log(data);

                    var artistsHTML =  newArtistHTMLElement(0, data.artist.name, data.artist.image[1]['#text'], 'out');

                    $('#myLineUp').append(artistsHTML);
                });
            }


        };

        function newArtistHTMLElement(index, name, img, status){
            var artistString = '<div class="media col-md-3 ' + status + '" id="' + index + '">' +
                '<div class="media-wrapper">' +
                '<div class="media-left media-middle">' +
                '<img class="media-object img-circle" src="' + img + '" alt="' + name + '">' +
                '</div>' +
                '<div class="media-body">' +
                '<h4 class="media-heading">' + name + '</h4>' +
                '</div>' +
                '</div>' +
                '</div>';

            return artistString
        }

    });
})();
