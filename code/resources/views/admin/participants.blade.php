@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Participants</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('/exportxls') }}" class="btn btn-default" aria-label="Left Align">
                    <span class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></span>
                    Exporteer xls
                </a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <tr>
                        <th>Naam</th>
                        <th>Email</th>
                        <th>Ip address</th>
                        <th></th>
                    </tr>

                    @foreach($participants as $participant)
                        <tr>
                            <td>
                                @if(is_null($participant->last_name))
                                    {{ $participant->first_name }}
                                @else
                                    {{ $participant->first_name . ' ' .  $participant->last_name}}
                                @endif

                            </td>
                            <td>{{ $participant->email }}</td>
                            <td>{{ $participant->ip_address }}</td>
                            <td>
                                @if ( is_null($participant->deleted_at))
                                    <a href="{{ url('/toggle', $participant->id) }}" class="btn btn-default" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                @else
                                    <a href="{{ url('/toggle', $participant->id) }}" class="btn btn-default" aria-label="Left Align">
                                        <span class=" glyphicon glyphicon-repeat" aria-hidden="true"></span>
                                    </a>
                                @endif

                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
