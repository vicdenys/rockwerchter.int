<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
</head>

<body>

<div id="app">

    @if(!Request::is('/'))
        <div class="container-fluid top-bar">
            <div class="container">
                <div class="top-gradient-bar"></div>

                <div class="row ">
                    <div class="col-md-12 logo-bar">
                        <a href="/">
                            <img src="/img/logo_big.png" alt="logo rock werchter" class="img-circle logo">

                            <h1 class="title">Rock Werchter</h1>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    @endif

    @yield('content')
</div>

<!-- Scripts -->
<script src="/js/app.js"></script>
</body>
</html>
