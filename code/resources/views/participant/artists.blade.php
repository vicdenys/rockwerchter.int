@extends('layouts.p_app')

@section('content')
    <div class="container selectArtistWrapper">
        <div class="row">

            <div class="col-md-12">
                <h1 class="title">Hey {{ $participant['first_name'] }}! Creëer uw Line-up</h1>
                <p>Selecteer 8 artiesten die in uw line-up moeten staan.</p>
                <a href="{{ url('/cancelregister') }}" class="">
                    Ben je niet {{ $participant['first_name'] }}?
                </a>
            </div>


        </div>

        <div class="row ">
            <div class="col-md-12">
                <div class="line-up-wrapper">
                    <h3>Uw line-up</h3>

                    <div class="row">
                        <div class="col-md-12">
                            <div id="myLineUp">

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4 id="no-artist">U heeft nog geen artiesten geselecteerd. U moet 8 artiesten selecteren.</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 submit-wrapper">
                            <p>U zal geen aanpassingen meer kunnen maken aan uw line-up nadat u ze verzonden heeft.</p>
                            <button class="btn btn-default submit"  id="submit" disabled>
                                Verzenden
                            </button>
                        </div>

                    </div>
                </div>
            </div>


        </div>


        <div class="row" >
            <div class="col-md-12" id="artists">
                <h3>Artiesten</h3>

                <!-- POPULATE VIA JSON -->
            </div>


        </div>

            {!! Form::open(['url' => '/savep', 'method' => 'post', 'id' => 'artistForm']) !!}

                {!! Form::token() !!}

            {!! Form::close() !!}

    </div>


@stop