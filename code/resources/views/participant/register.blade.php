@extends('layouts.p_app')

@section('content')
    <div class="container-fluid register">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <h1 class="title">Super dat je mee doet!</h1>
                    <p>We hebben eerst enkele gegevens van je nodig.</p>
                </div>

                @if(!isset($providerUser))
                    <div class="col-md-12">
                        <a href="{{ route('getSocialAuth', 'facebook') }}" class="btn btn-default btn_Facebook">Facebook Login</a>
                    </div>
                @endif

                <h4>of</h4>

                @if (count($errors) > 0)
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                {!! Form::open(['url' => '/register', 'method' => 'post']) !!}

                {!! Form::token() !!}

                <div class="form-group {{ ($errors->first('first_name')) ? 'has-error' : '' }} ">
                    <div class="col-md-12">
                        {!! Form::text('first_name', old('first_name'), array('class' => 'form-control', 'placeholder' => 'Voornaam')) !!}
                    </div>
                </div>

                <div class="form-group {{ ($errors->first('last_name')) ? 'has-error' : '' }}">
                    <div class="col-md-12">
                        {!! Form::text('last_name', old('last_name'), array('class' => 'form-control', 'placeholder' => 'Naam')) !!}
                    </div>
                </div>

                <div class="form-group {{ ($errors->first('email')) ? 'has-error' : '' }}">
                    <div class="col-md-12">
                        {!! Form::text('email', old('email') , array('class' => 'form-control', 'placeholder' => 'E-mail')) !!}
                    </div>
                </div>

                <div class="form-group {{ ($errors->first('address')) ? 'has-error' : '' }}">
                    <div class="col-md-12">
                        {!! Form::text('address', old('address'), array('class' => 'form-control', 'placeholder' => 'Straat & Huisnummer')) !!}
                    </div>
                </div>

                <div class="form-group {{ ($errors->first('city')) ? 'has-error' : '' }}">
                    <div class="col-md-12">
                        {!! Form::text('city', old('city'), array('class' => 'form-control', 'placeholder' => 'Plaats')) !!}
                    </div>
                </div>

                <div class="col-md-12">
                    {!! Form::submit('Registreer', array('class' => 'btn submit', 'id' => 'btn_Register')) !!}
                </div>



                {!! Form::close() !!}
            </div>
        </div>
    </div>


@stop