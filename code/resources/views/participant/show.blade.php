@extends('layouts.p_app')

@section('content')
    <div class="container showParticipant">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title">Line-up van {{ $participant->first_name }}</h3>

                <div class="line-up-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="myLineUp">

                            </div>
                        </div>
                    </div>

                </div>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12">

                <h4 class="votes">{{ $votes }} stemmen</h4>
                @if(count($errors))
                    <p class="has-error">Je hebt al gestemd op deze line-up</p>
                @else
                    <a href="{{ route('getSocialVote', ['facebook', $participationId]) }}" class="btn btn-default btn_Facebook">Facebook Login & stem</a>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p>Help {{ $participant->first_name }} door deze link te delen met zoveel mogelijk mensen:</p>
                <a href="{{ Request::url()  }}">{{ Request::url()  }}</a>
            </div>
        </div>

        <script>

            function getArtists(){
                return artists = <?= json_encode($artists) ?>;
            }
        </script>

@stop