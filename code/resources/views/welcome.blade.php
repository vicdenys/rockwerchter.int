@extends('layouts.p_app')

@section('content')

    <div class="container-fluid welcome">
        <div class="container">
            <div class="top-gradient-bar"></div>

            <div class="row ">
                <div class="col-md-12 logo-bar">
                    <img src="/img/logo_big.png" alt="logo rock werchter" class="img-circle logo">

                    <h1 class="title">Rock Werchter</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 expl">
                    <h4>
                        Creëer je eigen line-up voor Rock Werchter 2017 en maak kans op 10 combi-tickets.
                    </h4>

                    <p>
                        Stel je eigen lijst op met artiesten die volgens jou zeker moeten spelen op Rock Werchter 2017.
                        Deel je eigengemaakte line-up met zoveel mogelijk vrienden en laat ze stemmen op jou voorstel.
                        De line-up met de meeste stemmen wint. Misschien wordt jou line-up wel werkelijkheid!
                        Deze wedstrijd loopt in vier perioden waarbij elke keer een winnaar wordt gekozen.
                    </p>

                    @if(is_null($currentPeriod))
                        <p>De wedstrijd is even gepauseerd en zal spoedig terug starten</p>
                    @else
                        <a href="{{ url('/register') }}" class="btn btn-primary">
                            Deelnemen
                        </a>
                    @endif

                </div>

            </div>
        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Winnaar vorige periode</h2>
                @if(!is_null($participant))
                    <h3>Proficiat {{ $participant->first_name }}! U kan samen met 10 vrienden naar Rock Werchter 2017</h3>
                    <a href="{{ url('participant', $participantPeriodId) }}">check zijn line-up</a>
                @else
                    <p>Er was geen winnaar vorige periode</p>
                @endif
            </div>
        </div>
    </div>
@stop
