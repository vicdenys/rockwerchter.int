<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

$this->get('/', 'UserController@index');

// Participant Routes
$this->get('register', 'ParticipantController@showRegisterForm');
$this->post('register', 'ParticipantController@register');
$this->get('artists', ['uses' => 'ParticipantController@showArtistForm', 'as' => 'showArtistForm']);
$this->get('cancelregister', 'ParticipantController@cancelRegister');
$this->post('savep', 'ParticipantController@saveParticipant');
$this->get('participant/{id}', ['uses' =>'ParticipantController@show', 'as' => 'show']);

// Social | voting
Route::get('/socialvote/{provider?}/{post}',['uses' => 'AuthController@getSocialVote', 'as'   => 'getSocialVote']);



// Auth
// Login Routes
$this->get('login', 'Auth\LoginController@showLoginForm');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout');

//Social Login
$this->get('/sociallogin/{provider?}',['uses' => 'AuthController@getSocialAuth', 'as'   => 'getSocialAuth']);
$this->get('/callback/{provider?}',[ 'uses' => 'AuthController@getSocialAuthCallback', 'as'   => 'getSocialAuthCallback']);



$this->get('/admin', 'HomeController@index');
$this->get('/toggle/{id}', 'HomeController@toggleParticipant');
$this->get('/exportxls', 'HomeController@participants');






